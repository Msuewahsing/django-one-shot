from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


def todo_list_list(request):
    todolist = TodoList.objects.all()

    context = {
        "todo_list": todolist,
    }

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todoitem = TodoList.objects.get(id=id)
    context = {
        "todo_list_detail": todoitem,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
        return redirect("todo_list_detail", id=model_instance.id)

    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():

            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.id)

    else:
        form = TodoListForm(instance=model_instance)
    context = {
        "edit": model_instance,
        "form": form,

        }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":

        model_instance.delete()
        return redirect("/todos/")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            model_instance = form.save()
            return redirect("todo_list_detail", id=model_instance.list.id)

    else:

        form = TodoItemForm()

    context = {
        "form": form
    }
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    model_instance = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=model_instance)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoItemForm(instance=model_instance)
    context = {
        "edit": model_instance,
        "form": form
    }
    return render(request, "todos/items/edit.html", context)
